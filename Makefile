SHELL = /bin/bash

build_tag ?= yard-server

.PHONY: build
build:
	docker build -t $(build_tag) - < ./Dockerfile

.PHONY: clean
clean:
	echo 'no-op'

.PHONY: test
test:
	./test.sh
