#!/bin/bash -eu

docker-compose up -d

container="yard-server_app_1"

code=1
interval=5
timeout=60

SECONDS=0
while [[ $SECONDS -lt $timeout ]]; do
    echo -n "Checking health status ... "
    status=$(docker inspect -f '{{.State.Health.Status}}' $container)
    echo $status

    case $status in
        healthy)
	    code=0
            break
            ;;
        unhealthy)
            break
            ;;
        starting)
            sleep $interval
            ;;
        *)
            echo "Unexpected status."
            break
            ;;
    esac
done

echo "Cleaning up ..."
docker-compose down

if [[ $code -eq 0 ]]; then
    echo "SUCCESS"
else
    echo "FAILURE"
fi

exit $code
