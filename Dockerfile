FROM ruby:2.7

SHELL ["/bin/bash", "-c"]

ENV APP_USER="app-user" \
	APP_UID="1001" \
	APP_GID="0" \
	LANG="en_US.UTF-8" \
	LANGUAGE="en_US:en" \
	TZ="US/Eastern"

RUN echo "$LANG UTF-8" >> /etc/locale.gen; \
	locale-gen $LANG; \
	gem install yard

WORKDIR /docs

VOLUME /docs

RUN useradd -r -u $APP_UID -g $APP_GID -d /docs -s /sbin/nologin $APP_USER; \
	chown -R $APP_UID:$APP_GID $GEM_HOME /docs

USER $APP_USER

EXPOSE 8080

ENTRYPOINT ["yard", "server", "-B", "0.0.0.0", "-p", "8080"]
